#include "DialogWindow.h"
#include "ui_DialogWindow.h"
#include <math.h>


DialogWindow::MandelbrotValuesEditingSettings::MandelbrotValuesEditingSettings() :
    m_inputValidator(MIN_REAL_AND_IMAGINARY_VALUE, MAX_REAL_AND_IMAGINARY_VALUE, MANDELBROT_INPUT_PRECISION),
    m_textLocale(QLocale::English)
{
    m_textLocale.setNumberOptions(QLocale::RejectGroupSeparator);
    m_inputValidator.setLocale(m_textLocale);
}

DialogWindow::MandelbrotValuesEditingSettings::~MandelbrotValuesEditingSettings()
{

}

DialogWindow::DialogWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogWindow),
    m_mandelbrotGraphicsScene(this),
    m_mandelbrotValuesEditingSettings()
{
    if (0 != ui)
    {
        ui->setupUi(this);
        if (0 != ui->mandelbrotGraphicView)
        {
            ui->mandelbrotGraphicView->setScene(&m_mandelbrotGraphicsScene);
            m_mandelbrotGraphicsScene.calculateMandelbrot();
            m_mandelbrotGraphicsScene.drawMandelbrot();
        }

        setInputTextValidation();

        // Signals and slots
        QObject::connect(ui->editBox_minReal, SIGNAL(textChanged(QString)),
                         this, SLOT(onMandelbrotValuesEdited(QString)));

        QObject::connect(&m_mandelbrotGraphicsScene, SIGNAL(zoomChanged(double,double,double,double)),
                         this, SLOT(onZoomChanged(double,double,double,double)));

        onZoomChanged(m_mandelbrotGraphicsScene.getMinReal(),
                      m_mandelbrotGraphicsScene.getMaxReal(),
                      m_mandelbrotGraphicsScene.getMinImaginary(),
                      m_mandelbrotGraphicsScene.getMaxImaginary());
    }
}

DialogWindow::~DialogWindow()
{
    delete ui;
}

void DialogWindow::onZoomChanged(double a_minReal, double a_maxReal, double a_minImaginary, double a_maxImaginary)
{
    if (0 != ui)
    {
        if ((0 != ui->editBox_minReal) &&
            (0 != ui->editBox_maxReal) &&
            (0 != ui->editBox_minImaginary) &&
            (0 != ui->editBox_maxImaginary))
        {
            QString text;
            text.setNum(a_minReal, 'g', MANDELBROT_INPUT_PRECISION);
            ui->editBox_minReal->setText(text);

            text.setNum(a_maxReal, 'g', MANDELBROT_INPUT_PRECISION);
            ui->editBox_maxReal->setText(text);

            text.setNum(a_minImaginary, 'g', MANDELBROT_INPUT_PRECISION);
            ui->editBox_minImaginary->setText(text);

            text.setNum(a_maxImaginary, 'g', MANDELBROT_INPUT_PRECISION);
            ui->editBox_maxImaginary->setText(text);
        }
    }
}

void DialogWindow::onMandelbrotValuesEdited(QString a_text)
{
    if (0 != ui)
    {
        if (0 != ui->editBox_minReal)
        {
            if (0 != ui->editBox_minReal->validator())
            {
                int pos = 0;
                if (QValidator::Acceptable != ui->editBox_minReal->validator()->validate(a_text, pos))
                {
                    ui->editBox_minReal->setStyleSheet("color: red");
                }
                else
                {
                    ui->editBox_minReal->setStyleSheet("color: black");
                }
            }
        }
    }
}

void DialogWindow::paintEvent(QPaintEvent *a_event)
{
    //drawMandelbrot();
    //QPainter painter(this);
    //painter.drawRect(rect);
}

void DialogWindow::on_pushBtn_update_clicked()
{
    //if ( ui->editBox_minReal->validator()->validate()
}

void DialogWindow::on_pushBtn_switchColors_clicked()
{
    int multiplierFunction = m_mandelbrotGraphicsScene.getMultiplierFunction();
    multiplierFunction++;

    if (N_MULTIPLIER_FUNCTIONS_COUNT <= multiplierFunction)
    {
        multiplierFunction = N_MULTIPLIER_NONE;
    }
    m_mandelbrotGraphicsScene.setMultiplierFunction(static_cast<e_nMultiplierFunctions>(multiplierFunction));

    m_mandelbrotGraphicsScene.calculateMandelbrot();
    m_mandelbrotGraphicsScene.drawMandelbrot();
    m_mandelbrotGraphicsScene.update();
}

void DialogWindow::on_pushBtn_reset_clicked()
{
    m_mandelbrotGraphicsScene.resetMandelbrotPosition();
}

void DialogWindow::setInputTextValidation()
{
    if (0 != ui)
    {
        if ((0 != ui->editBox_minReal) &&
            (0 != ui->editBox_maxReal) &&
            (0 != ui->editBox_minImaginary) &&
            (0 != ui->editBox_maxImaginary))
        {
            ui->editBox_minReal->setValidator(&m_mandelbrotValuesEditingSettings.m_inputValidator);
            ui->editBox_maxReal->setValidator(&m_mandelbrotValuesEditingSettings.m_inputValidator);
            ui->editBox_minImaginary->setValidator(&m_mandelbrotValuesEditingSettings.m_inputValidator);
            ui->editBox_maxImaginary->setValidator(&m_mandelbrotValuesEditingSettings.m_inputValidator);
        }
    }
}
