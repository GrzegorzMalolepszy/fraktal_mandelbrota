#include "Mandelbrot.h"
#include "Constants.h"
#include <math.h>
#include <iostream>

Mandelbrot::Mandelbrot() :
    m_maxN(DEF_MAX_N),
    m_minReal(DEF_MIN_REAL),
    m_maxReal(DEF_MAX_REAL),
    m_minImaginary(DEF_MIN_IMAGINARY),
    m_maxImaginary(DEF_MAX_IMAGINARY),
    m_zoomValue(DEF_ZOOM_VALUE)
{

}

Mandelbrot::~Mandelbrot()
{

}

double Mandelbrot::mapToReal(int a_x)
{
    double range = m_maxReal - m_minReal;

    return a_x * (range / MANDELBROT_IMG_WIDTH) + m_minReal;
}

double Mandelbrot::mapToImaginary(int a_y)
{
    double range = m_maxImaginary - m_minImaginary;

    return a_y * (range / MANDELBROT_IMG_HEIGHT) + m_minImaginary;
}

int Mandelbrot::findMandelbrot(double a_cr, double a_ci)
{
    int i = 0;
    double zr = 0.0;
    double zi = 0.0;

    while ((i < m_maxN) && (zr * zr + zi * zi < 4.0))
    {
        double temp = zr * zr - zi * zi + a_cr;
        zi = 2.0 * zr * zi + a_ci;
        zr = temp;
        i++;
    }

    return i;
}

void Mandelbrot::zoom(double a_zoomValue, double a_shiftOnRealAxis, double a_shiftOnImaginaryAxis)
{
    double oldRangeOnRealAxis      = m_maxReal - m_minReal;
    double oldRangeOnImaginaryAxis = m_maxImaginary - m_minImaginary;

    double newRangeOnRealAxis      = oldRangeOnRealAxis * a_zoomValue;
    double newRangeOnImaginaryAxis = oldRangeOnImaginaryAxis * a_zoomValue;

    // Zooming in or out
    m_minReal      += (oldRangeOnRealAxis - newRangeOnRealAxis) / 2;
    m_maxReal      -= (oldRangeOnRealAxis - newRangeOnRealAxis) / 2;
    m_minImaginary += (oldRangeOnImaginaryAxis - newRangeOnImaginaryAxis) / 2;
    m_maxImaginary -= (oldRangeOnImaginaryAxis - newRangeOnImaginaryAxis) / 2;

    // Shifting
    m_minReal      += oldRangeOnRealAxis / 2 * a_shiftOnRealAxis;
    m_maxReal      += oldRangeOnRealAxis / 2 * a_shiftOnRealAxis;
    m_minImaginary += oldRangeOnImaginaryAxis / 2 * a_shiftOnImaginaryAxis;
    m_maxImaginary += oldRangeOnImaginaryAxis / 2 * a_shiftOnImaginaryAxis;
}

void Mandelbrot::resetToDefault()
{
    m_minReal = DEF_MIN_REAL;
    m_maxReal = DEF_MAX_REAL;
    m_minImaginary = DEF_MIN_IMAGINARY;
    m_maxImaginary = DEF_MAX_IMAGINARY;
}

int Mandelbrot::getMaxN()
{
    return m_maxN;
}

double Mandelbrot::getMinReal()
{
    return m_minReal;
}

double Mandelbrot::getMaxReal()
{
    return m_maxReal;
}

double Mandelbrot::getMinImaginary()
{
    return m_minImaginary;
}

double Mandelbrot::getMaxImaginary()
{
    return m_maxImaginary;
}
