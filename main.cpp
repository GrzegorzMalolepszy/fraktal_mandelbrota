#include <QApplication>
#include "DialogWindow.h"
#include "Constants.h"

int main(int argc, char *argv[])
{
    QApplication application(argc, argv);

    DialogWindow mainWindow;
    mainWindow.setFixedSize(MANDELBROT_IMG_WIDTH + OPTIONS_AREA_WIDTH, MANDELBROT_IMG_HEIGHT);
    mainWindow.show();

    return application.exec();
}
