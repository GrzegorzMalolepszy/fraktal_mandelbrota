#ifndef MANDELBROTGRAPHIC_H
#define MANDELBROTGRAPHIC_H

#include <QGraphicsScene>
#include "Mandelbrot.h"

enum e_nMultiplierFunctions
{
    N_MULTIPLIER_NONE,
    N_MULTIPLIER_RGB_X10,
    N_MULTIPLIER_R_X10,
    N_MULTIPLIER_G_X10,
    N_MULTIPLIER_B_X10,
    N_MULTIPLIER_RGB_N2,
    N_MULTIPLIER_R_N2,
    N_MULTIPLIER_G_N2,
    N_MULTIPLIER_B_N2,
    //N_MULTIPLIER_SIN,
    N_MULTIPLIER_FUNCTIONS_COUNT
};

class MandelbrotGraphic : public QGraphicsScene,
                          public Mandelbrot
{
    Q_OBJECT

public:
    /// constructor
    MandelbrotGraphic(QObject *parent);

    /// destructor
    ~MandelbrotGraphic();

    /// calculates Mandelbrot's fractal and saves color of every point to m_mandelbrotImage
    void calculateMandelbrot();

    /// draws Mandelbrot's Fractal
    void drawMandelbrot();

    /// Sets multiplier function for n
    void setMultiplierFunction(e_nMultiplierFunctions a_multiplierFunction);

    /// Gets multiplier function for n
    e_nMultiplierFunctions getMultiplierFunction();

    ///
    void resetMandelbrotPosition();

signals:
    void zoomChanged(double a_minReal, double a_maxReal, double a_minImaginary, double a_maxImaginary);

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    /// Mandelbrot's fractal image
    QImage m_mandelbrotImage;

    ///
    e_nMultiplierFunctions m_nMultiplierFunction;

    ///
    void getColorForPoint(int a_n, int & a_red, int & a_green, int & a_blue);

    ///
    int truncateWrongRGB(int a_colorComponent);
};

#endif // MANDELBROTGRAPHIC_H
