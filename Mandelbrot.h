#ifndef MANDELBROT_H
#define MANDELBROT_H

class Mandelbrot
{
public:
    /// Constructor
    Mandelbrot();

    /// Destructor
    ~Mandelbrot();

    /// Maps real numbers from Mandelbrot's set to imaginary numbers
    double mapToReal(int a_x);

    /// Maps imaginary numbers from Mandelbrot's set to real numbers
    double mapToImaginary(int a_y);

    /// Returns n
    int findMandelbrot(double a_cr, double a_ci);

    ///
    void zoom(double a_zoomValue, double a_shiftOnRealAxis = 0, double a_shiftOnImaginaryAxis = 0);

    ///
    void resetToDefault();

    ///
    int getMaxN();

    ///
    double getMinReal();

    ///
    double getMaxReal();

    ///
    double getMinImaginary();

    ///
    double getMaxImaginary();


protected:
    ///
    int m_maxN;

    ///
    double m_minReal;

    ///
    double m_maxReal;

    ///
    double m_minImaginary;

    ///
    double m_maxImaginary;

    /// zoom in/out value
    /// need to be <= 1.0
    double m_zoomValue;
};

#endif // MANDELBROT_H
