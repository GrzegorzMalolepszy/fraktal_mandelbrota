#include <QGraphicsSceneMouseEvent>
#include "MandelbrotGraphic.h"
#include "Constants.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

MandelbrotGraphic::MandelbrotGraphic(QObject *parent) :
    QGraphicsScene(parent),
    m_mandelbrotImage(MANDELBROT_IMG_WIDTH, MANDELBROT_IMG_HEIGHT, QImage::Format_RGB32),
    m_nMultiplierFunction(N_MULTIPLIER_NONE)
{
    m_mandelbrotImage.fill(0);
    srand( time( NULL ) );
}

MandelbrotGraphic::~MandelbrotGraphic()
{

}

void MandelbrotGraphic::calculateMandelbrot()
{
    double cr = 0;
    double ci = 0;
    int n = 0;

    // RGB color
    int r = 0;
    int g = 0;
    int b = 0;

    for (int y  = 0; y < MANDELBROT_IMG_WIDTH; y++)
    {
        for (int x = 0; x < MANDELBROT_IMG_HEIGHT; x++)
        {
            cr = mapToReal(x);
            ci = mapToImaginary(y);
            n  = findMandelbrot(cr, ci);

            getColorForPoint(n, r, g, b);
            m_mandelbrotImage.setPixel(x, y, qRgb(r, g, b));
        }
    }
}

void MandelbrotGraphic::drawMandelbrot()
{
    this->addPixmap(QPixmap::fromImage(m_mandelbrotImage));
}

void MandelbrotGraphic::setMultiplierFunction(e_nMultiplierFunctions a_multiplierFunction)
{
    m_nMultiplierFunction = a_multiplierFunction;
}

e_nMultiplierFunctions MandelbrotGraphic::getMultiplierFunction()
{
    return m_nMultiplierFunction;
}

void MandelbrotGraphic::resetMandelbrotPosition()
{
    resetToDefault();
    calculateMandelbrot();
    drawMandelbrot();
    emit zoomChanged(m_minReal, m_maxReal, m_minImaginary, m_maxImaginary);
}

void MandelbrotGraphic::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    double shiftOnRealAxis = (event->scenePos().x() - static_cast<double>(MANDELBROT_IMG_WIDTH / 2)) / static_cast<double>(MANDELBROT_IMG_WIDTH / 2);
    double shiftOnImaginaryAxis = (event->scenePos().y() - static_cast<double>(MANDELBROT_IMG_HEIGHT/ 2)) / static_cast<double>(MANDELBROT_IMG_HEIGHT / 2);

    if (event->button() == Qt::LeftButton)
    {
        zoom(m_zoomValue, shiftOnRealAxis, shiftOnImaginaryAxis);
        calculateMandelbrot();
        drawMandelbrot();
        emit zoomChanged(m_minReal, m_maxReal, m_minImaginary, m_maxImaginary);
    }
    else if (event->button() == Qt::RightButton)
    {
        zoom(1.0/m_zoomValue, shiftOnRealAxis, shiftOnImaginaryAxis);
        calculateMandelbrot();
        drawMandelbrot();
        emit zoomChanged(m_minReal, m_maxReal, m_minImaginary, m_maxImaginary);
    }
}

void MandelbrotGraphic::getColorForPoint(int a_n, int & a_red, int & a_green, int & a_blue)
{
    switch (m_nMultiplierFunction)
    {
        case N_MULTIPLIER_NONE:
        default:
        {
            a_red   = (a_n % MAX_BITS_PER_RGB_COLOR);
            a_green = (a_n % MAX_BITS_PER_RGB_COLOR);
            a_blue  = (a_n % MAX_BITS_PER_RGB_COLOR);
            break;
        }
        case N_MULTIPLIER_RGB_X10:
        {
            a_red   = ((a_n * 10) % MAX_BITS_PER_RGB_COLOR);
            a_green = ((a_n * 10) % MAX_BITS_PER_RGB_COLOR);
            a_blue  = ((a_n * 10) % MAX_BITS_PER_RGB_COLOR);
            break;
        }
        case N_MULTIPLIER_R_X10:
        {
            a_red   = ((a_n * 10) % MAX_BITS_PER_RGB_COLOR);
            a_green = (a_n % MAX_BITS_PER_RGB_COLOR);
            a_blue  = (a_n % MAX_BITS_PER_RGB_COLOR);
            break;
        }
        case N_MULTIPLIER_G_X10:
        {
            a_red   = (a_n % MAX_BITS_PER_RGB_COLOR);
            a_green = ((a_n * 10) % MAX_BITS_PER_RGB_COLOR);
            a_blue  = (a_n % MAX_BITS_PER_RGB_COLOR);
            break;
        }
        case N_MULTIPLIER_B_X10:
        {
            a_red   = (a_n % MAX_BITS_PER_RGB_COLOR);
            a_green = (a_n % MAX_BITS_PER_RGB_COLOR);
            a_blue  = ((a_n * 10) % MAX_BITS_PER_RGB_COLOR);
            break;
        }
        case N_MULTIPLIER_RGB_N2:
        {
            a_red   = ((a_n * a_n) % MAX_BITS_PER_RGB_COLOR);
            a_green = ((a_n * a_n) % MAX_BITS_PER_RGB_COLOR);
            a_blue  = ((a_n * a_n) % MAX_BITS_PER_RGB_COLOR);
            break;
        }
        case N_MULTIPLIER_R_N2:
        {
            a_red   = ((a_n * a_n) % MAX_BITS_PER_RGB_COLOR);
            a_green = (a_n % MAX_BITS_PER_RGB_COLOR);
            a_blue  = (a_n % MAX_BITS_PER_RGB_COLOR);
            break;
        }
        case N_MULTIPLIER_G_N2:
        {
            a_red   = (a_n % MAX_BITS_PER_RGB_COLOR);
            a_green = ((a_n * a_n) % MAX_BITS_PER_RGB_COLOR);
            a_blue  = (a_n % MAX_BITS_PER_RGB_COLOR);
            break;
        }
        case N_MULTIPLIER_B_N2:
        {
            a_red   = (a_n % MAX_BITS_PER_RGB_COLOR);
            a_green = (a_n % MAX_BITS_PER_RGB_COLOR);
            a_blue  = ((a_n * a_n) % MAX_BITS_PER_RGB_COLOR);
            break;
        }
        /*case N_MULTIPLIER_SIN:
        {
            a_red   = (static_cast<int>(a_n * sinf(a_n)) % MAX_BITS_PER_RGB_COLOR);
            a_green = (static_cast<int>(a_n * a_n) % MAX_BITS_PER_RGB_COLOR);
            a_blue  = (static_cast<int>(a_n * sinf(a_n)) % MAX_BITS_PER_RGB_COLOR);
            break;
        }*/
    }

    truncateWrongRGB(a_red);
    truncateWrongRGB(a_green);
    truncateWrongRGB(a_blue);
}

int MandelbrotGraphic::truncateWrongRGB(int a_colorComponent)
{
    int truncatedColorComponent = 0;

    if (0 > a_colorComponent)
    {
        truncatedColorComponent = 0;
    }
    else if ((MAX_BITS_PER_RGB_COLOR - 1) < a_colorComponent)
    {
        truncatedColorComponent = (MAX_BITS_PER_RGB_COLOR - 1);
    }

    return truncatedColorComponent;
}
