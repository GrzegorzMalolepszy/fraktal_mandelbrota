#ifndef CONSTANTSeee_H
#define CONSTANTSeee_H

/// Mandelbrot's image size
const int MANDELBROT_IMG_WIDTH = 600;
const int MANDELBROT_IMG_HEIGHT = 600;

/// Size of options area
const int OPTIONS_AREA_WIDTH = 250;

/// Default parameters of Mandelbrot's set
const int    DEF_MAX_N = 255;
const double DEF_MIN_REAL = -1.5;
const double DEF_MAX_REAL = 0.7;
const double DEF_MIN_IMAGINARY = -1.0;
const double DEF_MAX_IMAGINARY = 1.0;

///
const double DEF_ZOOM_VALUE = 0.6;

/// Parameters of data inout for Mandelbrot's set
const int MANDELBROT_INPUT_PRECISION = 12;
const double MIN_REAL_AND_IMAGINARY_VALUE = -2.5;
const double MAX_REAL_AND_IMAGINARY_VALUE = 2.5;

///
const int MAX_N_MULTIPLIED = 65535;

///
const short MAX_BITS_PER_RGB_COLOR = 256;

#endif // CONSTANTSeee_H
