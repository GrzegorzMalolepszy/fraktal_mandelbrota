#-------------------------------------------------
#
# Project created by QtCreator 2018-06-14T10:44:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Fraktal_Mandelbrota
TEMPLATE = app


SOURCES += main.cpp\
        DialogWindow.cpp \
    Mandelbrot.cpp \
    MandelbrotGraphic.cpp

HEADERS  += DialogWindow.h \
    Constants.h \
    Mandelbrot.h \
    MandelbrotGraphic.h

FORMS    += DialogWindow.ui
