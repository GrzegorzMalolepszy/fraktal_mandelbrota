#ifndef DIALOGWINDOW_H
#define DIALOGWINDOW_H

#include <QDialog>
#include <QtGui>
#include <QGraphicsScene>
#include "Mandelbrot.h"
#include "MandelbrotGraphic.h"
#include "Constants.h"

namespace Ui {
class DialogWindow;
}

class DialogWindow : public QDialog
{
    Q_OBJECT

public:
    /// constructor
    explicit DialogWindow(QWidget *parent = 0);

    /// destructor
    ~DialogWindow();

protected:

private slots:
    void onZoomChanged(double a_minReal, double a_maxReal, double a_minImaginary, double a_maxImaginary);
    void onMandelbrotValuesEdited(QString a_text);

    void on_pushBtn_update_clicked();
    void on_pushBtn_switchColors_clicked();
    void on_pushBtn_reset_clicked();

private:
    ///
    class MandelbrotValuesEditingSettings
    {
    public:
        /// constructor
        MandelbrotValuesEditingSettings();

        /// destructor
        ~MandelbrotValuesEditingSettings();

        /// validator for inputed text
        QDoubleValidator m_inputValidator;

        /// text locale
        QLocale m_textLocale;
    };

    ///
    void setInputTextValidation();

    /// paint event
    void paintEvent(QPaintEvent *a_event);

    /// UI
    Ui::DialogWindow *ui;

    ///
    MandelbrotGraphic m_mandelbrotGraphicsScene;

    ///
    MandelbrotValuesEditingSettings m_mandelbrotValuesEditingSettings;
};

#endif // DIALOGWINDOW_H
